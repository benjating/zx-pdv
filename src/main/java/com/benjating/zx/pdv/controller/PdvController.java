package com.benjating.zx.pdv.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.script.ScriptException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.benjating.zx.pdv.bean.Address;
import com.benjating.zx.pdv.bean.Pdv;
import com.benjating.zx.pdv.bean.PdvList;
import com.benjating.zx.pdv.dao.PdvRepository;
import com.benjating.zx.pdv.service.PdvService;

/**
 * Main controller that exposes the PDV API
 * 
 * @author benja
 *
 */
@RestController
@RequestMapping("/pdv")
public class PdvController {
	@Autowired
	PdvRepository repository;
	@Autowired
	PdvService pdvService;

	/**
	 * Creates or updates the PDV
	 * 
	 * @param pdv
	 * @return the saved PDV
	 * @throws Exception
	 */
	@PostMapping("")
	public ResponseEntity<Object> savePdv(@Valid @RequestBody Pdv pdv) throws Exception {
		Pdv savedPdv = repository.save(pdv);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedPdv.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	/**
	 * Creates or updates a list of PDVs
	 * 
	 * @param pdvList
	 * @return the saved list
	 * @throws Exception
	 */
	@PostMapping("/list")
	public ResponseEntity<Object> createPdvList(@Valid @RequestBody PdvList pdvList) throws Exception {
		repository.saveAll(pdvList.getPdvs());

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();

		return ResponseEntity.created(location).build();
	}

	/**
	 * Returns all PDVs
	 * @return
	 */
	@GetMapping("")
	public List<Pdv> retrieveAllPdvs() {
		return repository.findAll();
	}

	/**
	 * Gets PDV by the given ID
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public Pdv getPdv(@PathVariable long id) {
		return repository.findById(id).get();
	}

	/**
	 * Makes the search of the nearest PDV that covers the given address
	 * @param address
	 * @return nearest PDV
	 * @throws ScriptException
	 * @throws IOException
	 */
	// TODO: talvez fazer um GET passando long e lat como parametro
	@PostMapping("/search")
	public ResponseEntity<Pdv> searchPdv(@RequestBody Address address) throws ScriptException, IOException {
		Pdv pdv = pdvService.findPdv(address);
		if (pdv == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok(pdv);
		}
	}

	/**
	 * Removes PDV
	 * @param id
	 */
	@DeleteMapping("/{id}")
	public void deletePdv(@PathVariable long id) {
		repository.deleteById(id);
	}

}
