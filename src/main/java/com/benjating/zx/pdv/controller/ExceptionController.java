package com.benjating.zx.pdv.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.benjating.zx.pdv.bean.ResultObject;

/**
 * Gives a better control of what is returned when an exception is thrown, and avoids generic 500 error messages
 * @author benja
 *
 */
@ControllerAdvice
@RestController
public class ExceptionController {

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<?> handleException(Exception e) {
		ResultObject response = null;

		// TODO: da pra melhorar o mapeamento dos erros com mensagens mais amigáveis
		if (e instanceof DataIntegrityViolationException) {
			DataIntegrityViolationException ex = (DataIntegrityViolationException) e;
			response = new ResultObject(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),
					ex.getMessage());
			return new ResponseEntity<ResultObject>(response, HttpStatus.BAD_REQUEST);
		}
		else if (e instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException ex = (MethodArgumentNotValidException) e;
			response = new ResultObject(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),
					ex.getBindingResult().getAllErrors().toString());
			return new ResponseEntity<ResultObject>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(new ResultObject(e), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
