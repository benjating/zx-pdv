package com.benjating.zx.pdv.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.benjating.zx.pdv.component.JsonParser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Represents the CoverageArea of the PDV entity
 * @author benja
 *
 */
@Entity
public class CoverageArea implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String type;

	@OneToOne(mappedBy = "coverageArea")
	private Pdv pdv;

	/**
	 * Coordinates is just for JSON representation. The data saved on DB is coordinatesData
	 */
	@JsonInclude()
	@Transient
	private List<List<List<List<Double>>>> coordinates;

	/**
	 * The coordinates as a string. It's easier to save it as a JSON String.
	 */
	@Column
	@Lob
	private String coordinatesData;

	public CoverageArea() {
	}

	public CoverageArea(String type, Pdv pdv, List<List<List<List<Double>>>> coordinates, String coordinatesData) {
		super();
		this.type = type;
		this.pdv = pdv;
		this.coordinates = coordinates;
		this.coordinatesData = coordinatesData;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<List<List<List<Double>>>> getCoordinates() {
		return JsonParser.parseCoordinates(coordinatesData);
	}

	public void setCoordinates(List<List<List<List<Double>>>> coordinates) {
		coordinatesData = coordinates.toString();
	}

}
