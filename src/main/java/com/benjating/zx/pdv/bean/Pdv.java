package com.benjating.zx.pdv.bean;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.benjating.zx.pdv.component.JsonParser;
import com.benjating.zx.pdv.enumeration.MultipartGeometryType;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Represents the PDV entity
 * @author benja
 *
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class Pdv {
	@Id
	private Long id;

	@Column(nullable = false)
	@NotNull(message = "tradingName can't be null")
	private String tradingName;

	@Column(nullable = false)
	@NotNull(message = "ownerName can't be null")
	private String ownerName;

	@NotNull(message = "document can't be null")
	@Column(nullable = false, unique = true)
	private String document; // cnpj

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "coverage_area_id", referencedColumnName = "id", nullable = false)
	@NotNull(message = "coverageArea can't be null")
	private CoverageArea coverageArea;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", referencedColumnName = "id", nullable = false)
	@NotNull(message = "address can't be null")
	private Address address;

	public Pdv() {

	}

	public Pdv(Long id, String tradingName, String ownerName, String document, String coverageArea,
			String address) {
		super();
		this.id = id;
		this.tradingName = tradingName;
		this.ownerName = ownerName;
		this.document = document;
		this.coverageArea = new CoverageArea(MultipartGeometryType.MultiPolygon.getType(), this,
				JsonParser.parseCoordinates(coverageArea), coverageArea);
		this.address = new Address(address, this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTradingName() {
		return tradingName;
	}

	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public CoverageArea getCoverageArea() {
		return coverageArea;
	}

	public void setCoverageArea(CoverageArea coverageArea) {
		this.coverageArea = coverageArea;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
