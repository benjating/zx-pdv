package com.benjating.zx.pdv.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.benjating.zx.pdv.component.JsonParser;
import com.benjating.zx.pdv.enumeration.GeometryPrimiteType;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents the Address part of the PDV entity
 * 
 * @author benja
 *
 */
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class Address {

	@JsonIgnore
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String type;
	@ElementCollection
	@Column
	private List<Double> coordinates;

	@JsonIgnore
	@OneToOne(mappedBy = "address")
	private Pdv pdv;

	public Address() {
	}

	public Address(String coordinates) {
		super();
		this.type = GeometryPrimiteType.Point.getType();
		this.coordinates = JsonParser.parseAddress(coordinates);
	}
	
	public Address(String coordinates, Pdv pdv) {
		super();
		this.type = GeometryPrimiteType.Point.getType();
		this.coordinates = JsonParser.parseAddress(coordinates);
		this.pdv = pdv;
	}
	
	public Address(String type, String coordinates, Pdv pdv) {
		super();
		this.type = type;
		this.coordinates = JsonParser.parseAddress(coordinates);
		this.pdv = pdv;
	}

	public Address(String type, List<Double> coordinates, Pdv pdv) {
		super();
		this.type = type;
		this.coordinates = coordinates;
		this.pdv = pdv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Double> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<Double> coordinates) {
		this.coordinates = coordinates;
	}

	public Pdv getPdv() {
		return pdv;
	}

	public void setPdv(Pdv pdv) {
		this.pdv = pdv;
	}

}
