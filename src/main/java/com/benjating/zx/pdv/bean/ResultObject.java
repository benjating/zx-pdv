package com.benjating.zx.pdv.bean;

import org.springframework.http.HttpStatus;

/**
 * Objeto genérico utilizado nos erros retornados pela API REST
 * @author benja
 */
public class ResultObject {

	private int status;
	private String error;
	private String message;

	public ResultObject(int status, String error, String message) {
		super();
		this.status = status;
		this.error = error;
		this.message = message;
	}

	public ResultObject(Exception e) {
		super();
		this.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.error = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
		this.message = e.getMessage();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "{\"status\":\"" + status + "\", \"error\":\"" + error + "\", \"message\":\"" + message + "\"}";
	}

}
