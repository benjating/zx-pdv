package com.benjating.zx.pdv.bean;

import java.util.List;

/**
 * This entity wraps the PDV list.
 * 
 * @author benja
 *
 */
public class PdvList {
	private List<Pdv> pdvs;

	public List<Pdv> getPdvs() {
		return pdvs;
	}

	public void setPdvs(List<Pdv> pdvs) {
		this.pdvs = pdvs;
	}

}
