package com.benjating.zx.pdv.component;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * This is a utility class to parse the JSON string into more complex objects.
 * @author benja
 *
 */
public class JsonParser {

	private static String OPEN_ARRAY = "[";
	private static String CLOSE_ARRAY = "]";
	private static String COMMA = ",";

	/**
	 * Parses the String into coordinates structure
	 * 
	 * @param coordinatesData
	 * @return
	 */
	public static List<List<List<List<Double>>>> parseCoordinates(String coordinatesData) {
		// tokenizes the string considering "[" , "]" and "," as separators
		StringTokenizer stkn = new StringTokenizer(coordinatesData.trim(), "[//]//,", true);
		Stack<List> stack = new Stack<List>();
		List currentList = null;
		
		// whenever OPEN_ARRAY is found, goes to the lower level. When CLOSE_ARRAY is found goes to the upper level
		while (true) {
			try {
				String token = stkn.nextToken();
				if (token.equals(OPEN_ARRAY)) {
					List list = new ArrayList();
					if (currentList != null) {
						stack.push(currentList);
						currentList.add(list);
					}
					currentList = list;
				} else if (token.equals(CLOSE_ARRAY)) {
					if (!stack.empty()) {
						currentList = stack.pop();
					}
				} else { // tries to parse the content. If it is not valid, like ',' or ' ', just skips
					try {
						double d = Double.parseDouble(token);
						currentList.add(d);
					} catch (NumberFormatException e) {
						continue;
					}
				}
			} catch (NoSuchElementException e) {
				break;
			}
		}

		return currentList;
	}

	/**
	 * Parses the JSON string as a Address structure
	 * 
	 * @param address
	 * @return
	 */
	public static List<Double> parseAddress(String address) {
		StringTokenizer stkn = new StringTokenizer(address.trim(), "[//]//,", false);
		List<Double> list = new ArrayList<Double>();
		while (true) {
			try {
				String token = stkn.nextToken();
				double d = Double.parseDouble(token);
				list.add(d);
			} catch (NoSuchElementException e) {
				break;
			}
		}
		
		return list;
	}
}
