package com.benjating.zx.pdv.enumeration;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *  Enumeration that represents the Geometry Primitive Types. Only Point will be used, omitting the others
 * @author benja
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GeometryPrimiteType {
	Point("Point");
	
	private String type;

	GeometryPrimiteType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
