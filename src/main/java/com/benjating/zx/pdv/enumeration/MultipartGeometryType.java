package com.benjating.zx.pdv.enumeration;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *  Enumeration that represents the Multipart Geometries. Only Multipolygon will be used, omitting the others
 * @author benja
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MultipartGeometryType {
	MultiPolygon("MultiPolygon");
	
	private String type;

	MultipartGeometryType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
