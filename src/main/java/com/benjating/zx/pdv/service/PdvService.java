package com.benjating.zx.pdv.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.benjating.zx.pdv.bean.Address;
import com.benjating.zx.pdv.bean.Pdv;
import com.benjating.zx.pdv.dao.PdvRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides the business logic related to PDV operations
 * 
 * @author benja
 *
 */
@Service
public class PdvService {

	Logger logger = LoggerFactory.getLogger(PdvService.class);

	@Autowired
	PdvRepository repository;
	ObjectMapper objectMapper = new ObjectMapper();
	ScriptEngine engine;

	public PdvService() throws ScriptException, IOException {
		// javascript library setup
		ScriptEngineManager manager = new ScriptEngineManager();
		engine = manager.getEngineByName("JavaScript");
		Resource resource = new ClassPathResource("d3-geo.min.js");
		engine.eval(new BufferedReader(new InputStreamReader(resource.getInputStream())));
	}

	/**
	 * Finds the nearest PDV that covers the given address
	 * 
	 * @param address
	 * @return nearest PDV if found. Null if not.
	 * @throws ScriptException
	 * @throws IOException
	 */
	public Pdv findPdv(@Valid Address address) throws ScriptException, IOException {

		Pdv selectedPdv = null;
		Double shortestDistance = Double.MAX_VALUE;
		String coordinates = address.getCoordinates().toString();
		for (Pdv pdv : repository.findAll()) {
			boolean coversArea = coversArea(coordinates, pdv);
			if (coversArea) {
				Double distance = getDistance(pdv.getAddress(), address);
				logger.debug("PDV: " + pdv.getTradingName() + " / Distance = " + distance);
				if (distance < shortestDistance) {
					shortestDistance = distance;
					selectedPdv = pdv;
				}
			}
		}

		return selectedPdv;
	}

	/**
	 * Evaluates if the given PDV covers the coordinates
	 * 
	 * @param engine
	 * @param coordinates
	 * @param pdv
	 * @return ture if it covers, false otherwise
	 * @throws JsonProcessingException
	 * @throws ScriptException
	 */
	private boolean coversArea(String coordinates, Pdv pdv)
			throws JsonProcessingException, ScriptException {
		String coverageArea = objectMapper.writeValueAsString(pdv.getCoverageArea());
		Boolean result = (Boolean) engine.eval("d3.geoContains(" + coverageArea + ", " + coordinates + ");");
		return result;
	}

	/**
	 * Calculates the distance between the two addresses
	 * 
	 * @param address1
	 * @param address2
	 * @return the distance
	 */
	private Double getDistance(Address address1, Address address2) {
		Double x1 = address1.getCoordinates().get(0);
		Double y1 = address1.getCoordinates().get(1);
		Double x2 = address2.getCoordinates().get(0);
		Double y2 = address2.getCoordinates().get(1);
		double z = Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2);
		double distance = Math.pow(z, 0.5);
		return distance;
	}
}
