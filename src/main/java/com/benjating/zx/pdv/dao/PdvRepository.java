package com.benjating.zx.pdv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.benjating.zx.pdv.bean.Pdv;

public interface PdvRepository extends JpaRepository<Pdv, Long> {

}
