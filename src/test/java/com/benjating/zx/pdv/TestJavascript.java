package com.benjating.zx.pdv;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Proof of concept of the d3-geo library
 * @author benja
 *
 */
public class TestJavascript {

	public static void main(String[] args) throws ScriptException, IOException, NoSuchMethodException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("JavaScript");
		
		// read script file
		engine.eval(Files.newBufferedReader(Paths.get("C:\\dev\\Library\\d3-geo\\d3-geo.min.js"), StandardCharsets.UTF_8));
		engine.eval("var object = { \"type\": \"Polygon\",  \"coordinates\": [ [[10, 10], [10, 40], [40, 40], [40, 10], [10, 10]]]};");
		Object result = engine.eval("d3.geoContains(object, [20,20]);");
		System.out.println("Result = " + result);
	}
}
