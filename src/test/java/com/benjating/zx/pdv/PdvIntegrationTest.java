package com.benjating.zx.pdv;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StopWatch;

import com.benjating.zx.pdv.bean.Address;
import com.benjating.zx.pdv.bean.Pdv;
import com.benjating.zx.pdv.bean.PdvList;
import com.benjating.zx.pdv.controller.PdvController;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PdvIntegrationTest {
	@Autowired
    private MockMvc mvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	PdvController service;
	
	/**
	 * Basic test with createPdv and getPdvByID
	 * @throws Exception
	 */
	@Test
	public void testInsertionAndSearchById() throws Exception {
		
		// check that is initially empty
	    mvc.perform(
	    	get("/pdv")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(status().isOk())
	      .andExpect(jsonPath("$", Matchers.hasSize(0)));
	    
		// create a PDV instance
		String coverageArea = "[[[[0,0],[0,20],[20,20],[20,0],[0,0]]]]";
		String address = "[5,5]";
		String ownerName = "Adega Osasco";
		Pdv pdv = new Pdv(1L, "Ze da Adega", ownerName, "02.453.716/000170", coverageArea, address);
	    
	    // insert the PDV
	    mvc.perform(
	    		post("/pdv")
	    	  .content(objectMapper.writeValueAsString(pdv))
	  	      .contentType(MediaType.APPLICATION_JSON))
	  	      .andExpect(status().isCreated());
	    
	    // Fetch the inserted PDV and check it's the same
		mvc.perform( 
					get("/pdv/{id}", 1)
	    	      .accept(MediaType.APPLICATION_JSON))
	    	      .andDo(print())
	    	      .andExpect(status().isOk())
	    	      .andExpect(jsonPath("$.ownerName", is(ownerName)))
	      		  .andExpect(jsonPath("$.id", is(1)));
//	    	      .andExpect(MockMvcResultMatchers.content().string(containsString(ownerName)));
	}
	
	/**
	 * Checks that it does not allow insertion of Pdv with the same CNPJ
	 * 
	 * @throws Exception
	 */
	@Test
	public void checkDuplicateCnpj() throws Exception {
		// create PDV1
		String coverageArea = "[[[[0,0],[0,20],[20,20],[20,0],[0,0]]]]";
		String address = "[5,5]";
		Pdv pdv1 = new Pdv(1L, "PDV1", "PDV1", "111", coverageArea, address);

		// create PDV2 with different CNPJ
		Pdv pdv2 = new Pdv(2L, "PDV2", "PDV2", "222", coverageArea, address);
		
		// create PDV3 with the same CNPJ
		Pdv pdv3 = new Pdv(3L, "PDV3", "PDV3", "222", coverageArea, address);
		
		// insert the PDV1: it is ok
		mvc.perform(
				post("/pdv")
				.content(objectMapper.writeValueAsString(pdv1))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		
		// insert the PDV2: it is ok
		mvc.perform(
				post("/pdv")
				.content(objectMapper.writeValueAsString(pdv2))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		
		// insert the PDV3: it is nok
		mvc.perform(
				post("/pdv")
				.content(objectMapper.writeValueAsString(pdv3))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	/**
	 * <pre>
	 * Given 2 simple coverage area, 2 squares, for simplicity, we will test the following scenarios:
	 * 1) a point that is the intersection area. Should return the nearest PDV.
	 * 2) a point that is only in square1. Should return PDV1.
	 * 3) a point that is only in square2. Should return PDV2.
	 * 4) a point that is not in any coverage area. Should return empty content.
	 * </pre>
	 * 
	 * @throws Exception
	 */
	@Test
	public void searchPdv() throws Exception {
		// create PDV1
		String coverageArea1 = "[[[[0,0],[0,20],[20,20],[20,0],[0,0]]]]";
		String address1 = "[5,5]";
		Integer pdv1_id = 1;
		Pdv pdv1 = new Pdv(pdv1_id.longValue(), "PDV1", "PDV1", "111", coverageArea1, address1);

		// create PDV2
		String coverageArea2 = "[[[[10,10],[10,30],[30,30],[30,10],[10,10]]]]";
		String address2 = "[20,20]";
		Integer pdv2_id = 2;
		Pdv pdv2 = new Pdv(pdv2_id.longValue(), "PDV2", "PDV2", "222", coverageArea2, address2);
		
		PdvList list = new PdvList();
		List<Pdv> pdvs = new ArrayList<Pdv>();
		pdvs.add(pdv1);
		pdvs.add(pdv2);
		list.setPdvs(pdvs);
		
		// insert the list of pdvs
		mvc.perform(
				post("/pdv/list")
				.content(objectMapper.writeValueAsString(list))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		

		// scenario 1:
		Address address = new Address("[15,15]");
		mvc.perform( 
					post("/pdv/search")
					.content(objectMapper.writeValueAsString(address))
					.contentType(MediaType.APPLICATION_JSON))
	    	       	.andExpect(status().isOk())
	    	       	.andExpect(jsonPath("$.id", is(pdv2_id)));


		// scenario 2:
		address = new Address("[1,1]");
		mvc.perform( 
					post("/pdv/search")
					.content(objectMapper.writeValueAsString(address))
					.contentType(MediaType.APPLICATION_JSON))
	    	       	.andExpect(status().isOk())
	    	       	.andExpect(jsonPath("$.id", is(pdv1_id)));
		
		// scenario 3:
		address = new Address("[25,25]");
		mvc.perform( 
					post("/pdv/search")
					.content(objectMapper.writeValueAsString(address))
					.contentType(MediaType.APPLICATION_JSON))
	    	       	.andExpect(status().isOk())
	    	       	.andExpect(jsonPath("$.id", is(pdv2_id)));
		
		// scenario 4:
		address = new Address("[25,5]");
		mvc.perform( 
					post("/pdv/search")
					.content(objectMapper.writeValueAsString(address))
					.contentType(MediaType.APPLICATION_JSON))
	    	       	.andExpect(status().isNoContent());
	}
	
	/**
	 * Test the search service when there's 10k PDV in DB
	 * 
	 * @throws Exception
	 */
	@Test
	public void loadTest() throws Exception {

		PdvList list = new PdvList();
		List<Pdv> pdvs = new ArrayList<Pdv>();
		list.setPdvs(pdvs);

		StopWatch watch = new StopWatch();

		watch.start();
		int threshold = 10000;
		
		// load test will add several square polygons, by adding 1 to X axis and Y axis on each iteration
		for (int i = 0; i < threshold; i++) {
			Integer low = i;
			Integer high = i + 20;
			Integer mean = i + 10;

			String coverageArea = MessageFormat.format("[[[[{0},{0}],[{0},{1}],[{1},{1}],[{1},{0}],[{0},{0}]]]]", low,
					high);
			String address = MessageFormat.format("[{0},{0}]", mean);
			Long id = ((Integer) i).longValue();
			Pdv pdv = new Pdv(id, "PDV" + i, "PDV" + i, "cnpj" + i, coverageArea, address);

			pdvs.add(pdv);
		}
		
		// insert the list of pdvs
		mvc.perform(
				post("/pdv/list")
				.content(objectMapper.writeValueAsString(list))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		
		// check the number of inserted PDVs
	    mvc.perform(
	    	get("/pdv")
	      .contentType(MediaType.APPLICATION_JSON))
	      .andExpect(status().isOk())
	      .andExpect(jsonPath("$", Matchers.hasSize(threshold)));

		watch.stop();
	    System.out.println(MessageFormat.format("Insertion of {0} elements took {1} ms", threshold, watch.getTotalTimeMillis()));

	    // performs the search
		watch.start();
	    Address address = new Address("[15,15]");
		mvc.perform( 
					post("/pdv/search")
					.content(objectMapper.writeValueAsString(address))
					.contentType(MediaType.APPLICATION_JSON))
	    	       	.andExpect(status().isOk());

		watch.stop();
		System.out.println(MessageFormat.format("Search took {1} ms", threshold, watch.getTotalTimeMillis()));
	}
}
